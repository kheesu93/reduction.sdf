#ifndef _______HS_____VERTEX______
#define _______HS_____VERTEX______

#include <vector>
#include "HS_common.h"

class Triangle;
using namespace std;

#include <cuda.h>
#include <cuda_runtime.h>
//#include <thrust/device_vector.h>
//#include <thrust/host_vector.h>

struct gpuVertex
{
	float			x, y, z;
	unsigned int	gpuNfid;
	//float			nx, ny, nz;

	__host__ __device__ gpuVertex& operator-(const gpuVertex& other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;
	
		return *this;
	}
};

class Vertex
{
public:
	
	float			x, y, z;
	unsigned char	r, g, b, a;

	unsigned int	vNfid;
	float			nx, ny, nz;

	//__host__ __device__ Vertex();
	//__host__ __device__ ~Vertex();
	

	vector<Triangle*> m_vpFaces;
	void AddNeighbor(Triangle * pTris);
	int NbPolygonNeighbor();
	Triangle * GetPolygonNeighbor(int i);
	
	//thrust::host_vector<Triangle*> h_vpFaces(m_vpFaces.size());

	__host__ __device__ Vertex& operator=(const Vertex& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
		r = other.r;
		g = other.g;
		b = other.b;
		a = other.a;
		return *this;
	}
};
#endif _______HS_____VERTEX______