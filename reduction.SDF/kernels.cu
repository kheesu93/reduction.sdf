#include "kernels.h"



__global__ void DistanceFromPointToTriangle(float * d_Dist2, char * d_Type, 
											float	Px, float Py, float Pz,  
											float * d_T0x, float * d_T0y, float * d_T0z, 
											float * d_T1x, float * d_T1y, float * d_T1z, 
											float * d_T2x, float * d_T2y, float * d_T2z, unsigned long long N)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	char region = -1;

	if(tid < N)
	{

		float T0x = d_T0x[tid]; float T0y = d_T0y[tid]; float T0z = d_T0z[tid]; 
		float T1x = d_T1x[tid]; float T1y = d_T1y[tid]; float T1z = d_T1z[tid]; 
		float T2x = d_T2x[tid]; float T2y = d_T2y[tid]; float T2z = d_T2z[tid]; 

		
		//MgcVector3 kDiff = rkTri.Origin() - rkPoint;
		float kDiffx = T0x - Px;
		float kDiffy = T0y - Py;
		float kDiffz = T0z - Pz;

		//float fA00 = rkTri.Edge0().SquaredLength();
		//float fA01 = rkTri.Edge0().Dot(rkTri.Edge1());
		//float fA11 = rkTri.Edge1().SquaredLength();
		//float fB0 = kDiff.Dot(rkTri.Edge0());
		//float fB1 = kDiff.Dot(rkTri.Edge1());
		//float fC = kDiff.SquaredLength();

		float fA00 =  (T1x -  T0x)*(T1x -  T0x) + (T1y -  T0y)*(T1y -  T0y) + (T1z -  T0z)*(T1z -  T0z);
		float fA01 = (T1x -  T0x)*(T2x -  T0x) + (T1y -  T0y)*(T2y -  T0y) + (T1z -  T0z)*(T2z -  T0z);
		float fA11 =(T2x -  T0x)*(T2x -  T0x) + (T2y -  T0y)*(T2y -  T0y) + (T2z -  T0z)*(T2z -  T0z);
		float fB0 =kDiffx*(T1x -  T0x) +kDiffy*(T1y -  T0y) + kDiffz*(T1z -  T0z);
		float fB1 =kDiffx*(T2x -  T0x) +kDiffy*(T2y -  T0y) + kDiffz*(T2z -  T0z);
		float fC = kDiffx*kDiffx+kDiffy*kDiffy+kDiffz*kDiffz;


		float fDet = fabs(fA00*fA11-fA01*fA01);
		float fS = fA01*fB1-fA11*fB0;
		float fT = fA01*fB0-fA00*fB1;
		float fSqrDist;

		if ( fS + fT <= fDet )
		{
			if ( fS < 0.0 )
			{	          
				if ( fT < 0.0 )  // region 4
				{
					region = 4;
					if ( fB0 < 0.0 )
					{
		                fT = 0.0;
			            if ( -fB0 >= fA00 )
				        {
					        fS = 1.0;
						    fSqrDist = fA00+2.0*fB0+fC;
						}
						else
						{
							fS = -fB0/fA00;
							fSqrDist = fB0*fS+fC;
						}
					}
					else
					{
						fS = 0.0;
						if ( fB1 >= 0.0 )
						{
							fT = 0.0;
							fSqrDist = fC;
						}
						else if ( -fB1 >= fA11 )
						{
	                        fT = 1.0;
		                    fSqrDist = fA11+2.0*fB1+fC;
			            }
				        else
					    {
	                        fT = -fB1/fA11;
						    fSqrDist = fB1*fT+fC;
		                }
			        }
				}
				else  // region 3
				{
					region = 3;
	                fS = 0.0;
		            if ( fB1 >= 0.0 )
			        {
				        fT = 0.0;
					    fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
					    fT = 1;
	                    fSqrDist = fA11+2.0*fB1+fC;
					}
					else
					{
	                    fT = -fB1/fA11;
		                fSqrDist = fB1*fT+fC;
			        }
				}
			}
			else if ( fT < 0.0 )  // region 5
			{
				region = 5;
				fT = 0.0;
				if ( fB0 >= 0.0 )
	            {
		            fS = 0.0;
			        fSqrDist = fC;
	            }
		        else if ( -fB0 >= fA00 )
			    {
				    fS = 1.0;
	                fSqrDist = fA00+2.0*fB0+fC;
		        }
			    else
				{
	                fS = -fB0/fA00;
		            fSqrDist = fB0*fS+fC;
			    }
			}
			else  // region 0
			{
				region = 0;

				// minimum at interior point
				float fInvDet = 1.0/fDet;
				fS *= fInvDet;
				fT *= fInvDet;
				fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
	                fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
		    }
		}
		else
		{
	        float fTmp0, fTmp1, fNumer, fDenom;

		    if ( fS < 0.0 )  // region 2
	        {
				region = 2;
		        fTmp0 = fA01 + fB0;
			    fTmp1 = fA11 + fB1;
				if ( fTmp1 > fTmp0 )
				{
					fNumer = fTmp1 - fTmp0;
	                fDenom = fA00-2.0*fA01+fA11;
		            if ( fNumer >= fDenom )
			        {
				        fS = 1.0;
					    fT = 0.0;
						fSqrDist = fA00+2.0*fB0+fC;
	                }
		            else
			        {
				        fS = fNumer/fDenom;
					    fT = 1.0 - fS;
	                    fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
		                    fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
			        }
	            }
			    else
		        {
					fS = 0.0;
					if ( fTmp1 <= 0.0 )
					{
	                    fT = 1.0;
		                fSqrDist = fA11+2.0*fB1+fC;
			        }
				    else if ( fB1 >= 0.0 )
	                {
		                fT = 0.0;
			            fSqrDist = fC;
				    }
					else
					{
	                    fT = -fB1/fA11;
		                fSqrDist = fB1*fT+fC;
			        }
				}
			}
			else if ( fT < 0.0 )  // region 6
			{
				region = 6;
				fTmp0 = fA01 + fB1;
		        fTmp1 = fA00 + fB0;
			    if ( fTmp1 > fTmp0 )
	            {
		            fNumer = fTmp1 - fTmp0;
			        fDenom = fA00-2.0*fA01+fA11;
				    if ( fNumer >= fDenom )
	                {
		                fT = 1.0;
			            fS = 0.0;
				        fSqrDist = fA11+2.0*fB1+fC;
					}
					else
					{
	                    fT = fNumer/fDenom;
		                fS = 1.0 - fT;
			            fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
				            fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
	                }
			    }
		        else
	            {
		            fT = 0.0;
			        if ( fTmp1 <= 0.0 )
				    {
					    fS = 1.0;
						fSqrDist = fA00+2.0*fB0+fC;
	                }
		            else if ( fB0 >= 0.0 )
			        {
				        fS = 0.0;
					    fSqrDist = fC;
					}
					else
					{
	                    fS = -fB0/fA00;
		                fSqrDist = fB0*fS+fC;
			        }
				}
			}
			else  // region 1
			{
				region = 1;

	            fNumer = fA11 + fB1 - fA01 - fB0;
		        if ( fNumer <= 0.0 )
			    {
				    fS = 0.0;
	                fT = 1.0;
		            fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
	                fDenom = fA00-2.0*fA01+fA11;
		            if ( fNumer >= fDenom )
			        {
				        fS = 1.0;
					    fT = 0.0;
	                    fSqrDist = fA00+2.0*fB0+fC;
		            }
			        else
				    {
						fS = fNumer/fDenom;
						fT = 1.0 - fS;
						fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
	                        fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
		            }
			    }
			}
		}

		d_Dist2[tid] = fabs(fSqrDist);
		d_Type[tid] = region;	
	}
	
}
