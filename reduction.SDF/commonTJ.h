#ifndef ___COME_ON_TJ___
#define ___COME_ON_TJ___

//#define __DOUBLE_PRE__
#ifdef __DOUBLE_PRE__
#define REAL		double
#else
#define REAL		float
#endif



#define KERNELID	5
#define	PRIMALTREE_LEVEL	7
#define	UINTEG	unsigned int	

#define SAFEDELETE(x)		if(x != NULL) delete x
#define SAFEDELETEARRAY(x)		if(x != NULL) delete[] x
#define SAFEFREE(x)		if(x != NULL) free(x)

typedef struct
{
	REAL			data;
	UINTEG		tid;
	char			type;
} SHAREDATA;

extern double ROOT_TABLE[8][3];



#endif