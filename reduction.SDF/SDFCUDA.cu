//****************************************************************************************
// Taejung Park (taejung.park@gmail.com)
// Created : 2010. 01. 09.
// Modified : 2017. 10. 18
// 
//****************************************************************************************
#define MAXLINE			1000
#define MAXFILENAME	255
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <thrust/sequence.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/extrema.h>
#include "kernels.h"

#include "SDFCUDA.h"
#include "tjvector_types.h"
#include "Mesh.h"
#include "Triangle.h"
#include "StopWatch.h"

#define	 ALLOWANCE		0.025	// for one partial direction. eg. total expansion to x axis = 0.025*2
#define	 NUMVOXEL_X	64	
#define	 NUMVOXEL_Y	64	
#define	 NUMVOXEL_Z	64	
#define G_BLOCKSIZE				512

#include <stdio.h>
#include <string.h>

struct find_min
{
	template <typename Tuple> 
	__host__ __device__
		Tuple operator()(const Tuple& a, const Tuple& b) const
	{
		if(thrust::get<0>(a) > thrust::get<0>(b))
			return b;
		else
			return a;
	}
};

unsigned int getNumQueries(const char * filepath)
{
	int num_queries;
	
	FILE * fp = fopen(filepath,"r");
	fscanf(fp, "%d\n", &num_queries);

	fclose(fp);
	return num_queries;
}




unsigned int checkAndLoadQueries(const char * filepath, float * qX, float * qY, float * qZ, bool bOnlyCheck)
{
	int num_queries;
	float x,y,z;
	
	FILE * fp = fopen(filepath,"r");
	fscanf(fp, "%d\n", &num_queries);
	tjtypes::fvec3 v3Max = tjtypes::make_fvec3(-1000000.0f,-1000000.0f,-1000000.0f);
	tjtypes::fvec3 v3Min = tjtypes::make_fvec3(1000000.0f,1000000.0f,1000000.0f);


	for(int i = 0; i < num_queries; i++)
	{
		fscanf(fp, "%f\t%f\t%f\n", &x, &y, &z);

		if(!bOnlyCheck)
		{
			qX[i] = x;
			qY[i] = y;
			qZ[i] = z;
		}
		if(x > v3Max.x)
			v3Max.x = x;
		if(y > v3Max.y)
			v3Max.y = y;
		if(z > v3Max.z)
			v3Max.z = z;

		if(x < v3Min.x)
			v3Min.x = x;
		if(y < v3Min.y)
			v3Min.y = y;
		if(z < v3Min.z)
			v3Min.z = z;

	}
	fclose(fp);
	printf("\n Min = (%f, %f, %f) ~ Max = (%f, %f, %f)\n",v3Min.x, v3Min.y, v3Min.z, v3Max.x, v3Max.y, v3Max.z);
	return num_queries;

}
void selectTheBestGPU(void)
{
		// select the best device
	int numDevices = 0;
	cudaDeviceProp props;
	cudaDeviceProp bestProps;

	int maxMultiprocessors = 0, maxDevice = 0;
	int device;
	cudaGetDeviceCount(&numDevices);
	printf("\n List of GPUs...");
	if (numDevices > 1) 
	{
		for (device=0; device<numDevices; device++) 
		{
			
			cudaGetDeviceProperties(&props, device);
			printf("\n - Device %d: %s, # of multiprocesseros = %d", device, props.name, props.multiProcessorCount);
			if (maxMultiprocessors < props.multiProcessorCount) 
			{
				bestProps = props;
				maxMultiprocessors = props.multiProcessorCount;
				maxDevice = device;
			}
		}
		cudaSetDevice(maxDevice);
		printf("\nUsing Device %d: %s", maxDevice, bestProps.name);
		printf("\nTotal Global memory %llu (MB)", bestProps.totalGlobalMem / (1<<20));
	}
}


unsigned int powui(unsigned int base, unsigned int expo)
{
	unsigned int result = 1;

	for(unsigned int i = 0 ; i < expo; i++)
	{
		result *= base;
	}

	return result;

}
unsigned int GetCloserPowerOfTwo(unsigned int N)
{
	unsigned int N2;

	float dN = log((float)N)/log(2.0);

	N2 = (unsigned int) dN;

	float dN2 = (float) N2;

	if(dN - dN2 > 0.0)
		N2 ++;


	return powui(2,N2);
}



void reductionSDF(const char * filepath, const char * outfilepath, float * qX, float * qY, float * qZ, 	
	float * outDistance,
	unsigned long long  width, unsigned long long length, unsigned long long height)
{

	selectTheBestGPU();

	unsigned long long i;
	
	char line[MAXLINE];
	char filename[MAXFILENAME];
	char outfilename[MAXFILENAME];
	unsigned int numVerts = 0;
	unsigned int numFaces = 0;
	unsigned int blocksize = G_BLOCKSIZE;
	
	dim3 block;
	dim3 grid;




	strcpy(filename, filepath);
	strcpy(outfilename, outfilepath);
	
	Mesh * ptriMesh = new Mesh();
	
	printf("Reading a mesh... : %s \n", filepath);

	ptriMesh->meshRead(filepath);

	printf("Converting the mesh... \n");

	ptriMesh->ConvertTo075Canonical();



	float * pTriX0array = NULL;
	float * pTriY0array = NULL;
	float * pTriZ0array = NULL;
	float * pTriX1array = NULL;
	float * pTriY1array = NULL;
	float * pTriZ1array = NULL;
	float * pTriX2array = NULL;
	float * pTriY2array = NULL;
	float * pTriZ2array = NULL;

	numFaces = (unsigned int) ptriMesh->m_numFaces;
	numVerts = (unsigned int) ptriMesh->m_numVerts;

	//numFaces = GetCloserPowerOfTwo(numFaces);

	block.x = blocksize;
	grid.x = (numFaces+block.x-1)/block.x;

	pTriX0array = new float[numFaces];
	pTriY0array = new float[numFaces];
	pTriZ0array = new float[numFaces];

	pTriX1array = new float[numFaces];
	pTriY1array = new float[numFaces];
	pTriZ1array = new float[numFaces];

	pTriX2array = new float[numFaces];
	pTriY2array = new float[numFaces];
	pTriZ2array = new float[numFaces];

	for(i=0; i<numFaces; i++)
	{
		pTriX0array[i] = pTriX1array[i] = pTriX2array[i] = 0.0;
		pTriY0array[i] = pTriY1array[i] = pTriY2array[i] = 0.0;
		pTriZ0array[i] = pTriZ1array[i] = pTriZ2array[i] = 0.0;

	}


	for(i=0; i<numFaces; i++)
	{
		Triangle T = ptriMesh->m_pFaces[i];

		Vertex v0 = ptriMesh->m_pVerts[T.m_ui[0]];
		Vertex v1 = ptriMesh->m_pVerts[T.m_ui[1]];
		Vertex v2 = ptriMesh->m_pVerts[T.m_ui[2]];
		

		pTriX0array[i] =  v0.x;
		pTriY0array[i] =  v0.y;
		pTriZ0array[i] =  v0.z;
		pTriX1array[i] =  v1.x;
		pTriY1array[i] =  v1.y;
		pTriZ1array[i] =  v1.z;
		pTriX2array[i] =  v2.x;
		pTriY2array[i] =  v2.y;
		pTriZ2array[i] =  v2.z;
				
	}
	float *d_T0x;
	float *d_T0y;
	float *d_T0z;
	float *d_T1x;
	float *d_T1y;
	float *d_T1z;
	float *d_T2x;
	float *d_T2y;
	float *d_T2z;
	char * d_type;
	float * d_distance;

	cudaMalloc((void **) &d_T0x, numFaces * sizeof(float));
	cudaMalloc((void **) &d_T0y, numFaces * sizeof(float));
	cudaMalloc((void **) &d_T0z, numFaces * sizeof(float));

	cudaMalloc((void **) &d_T1x, numFaces * sizeof(float));
	cudaMalloc((void **) &d_T1y, numFaces * sizeof(float));
	cudaMalloc((void **) &d_T1z, numFaces * sizeof(float));

	cudaMalloc((void **) &d_T2x, numFaces * sizeof(float));
	cudaMalloc((void **) &d_T2y, numFaces * sizeof(float));
	cudaMalloc((void **) &d_T2z, numFaces * sizeof(float));

	cudaMalloc((void **) &d_type, numFaces * sizeof(char));
	cudaMalloc((void **) &d_distance, numFaces * sizeof(float));

	cudaMemcpy(d_T0x,pTriX0array,numFaces * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_T0y,pTriY0array,numFaces * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_T0z,pTriZ0array,numFaces * sizeof(float),cudaMemcpyHostToDevice);

	cudaMemcpy(d_T1x,pTriX1array,numFaces * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_T1y,pTriY1array,numFaces * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_T1z,pTriZ1array,numFaces * sizeof(float),cudaMemcpyHostToDevice);

	cudaMemcpy(d_T2x,pTriX2array,numFaces * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_T2y,pTriY2array,numFaces * sizeof(float),cudaMemcpyHostToDevice);
	cudaMemcpy(d_T2z,pTriZ2array,numFaces * sizeof(float),cudaMemcpyHostToDevice);

	
	thrust::device_vector<char> thd_type (numFaces);
	thrust::device_vector<unsigned int> thd_index (numFaces);
	thrust::device_vector<float> thd_distance (numFaces);

	thrust::sequence(thd_index.begin(), thd_index.end());


	unsigned long long totalQueries = width * length * height;


	SHAREDATA result, re2;

	

	printf("calculating... \n");
//	float * distarry = new float[numFaces];

	unsigned long long count = 0;
	unsigned long long prev_count = 1;

	for(i=0;i<totalQueries;i++)
	{
		float x = qX[i];
		float y = qY[i];
		float z = qZ[i];
		count = 100*i/totalQueries;
		if(prev_count != count)
		{
			//printf("%d\t", 100*i/totalQueries);
			prev_count = count;
		}
		DistanceFromPointToTriangle<<<grid, block>>>(d_distance,d_type, x,y,z,  
											d_T0x, d_T0y, d_T0z,
											d_T1x, d_T1y, d_T1z,
											d_T2x, d_T2y, d_T2z,
											numFaces);
	
		float * distarry = new float[numFaces];;
		cudaMemcpy((void *)distarry, d_distance, numFaces * sizeof(float), cudaMemcpyDeviceToHost);

		/*
		FILE *f = fopen("dist.txt","w");
		for(int x = 0 ; x < numFaces; x++)
			fprintf(f,"\n%f", distarry[x]);
		fclose(f);
		*/
		
		thrust::copy(d_distance, d_distance + numFaces, thd_distance.begin());
		thrust::copy(d_type, d_type + numFaces, thd_type.begin());

	


		thrust::device_vector<float>::iterator iter =  thrust::min_element(thd_distance.begin(), thd_distance.end());
  
		unsigned int position = iter - thd_distance.begin();

		float min_dist = *iter;

		outDistance[i] = min_dist;

	//	printf("\n(%f,%f,%f) : pos = %u, dist = %f",x, y, z, position, outDistance[i]);
	
	}

//	delete[] distarry;



	




	SAFEDELETEARRAY(pTriX0array);
	SAFEDELETEARRAY(pTriY0array);
	SAFEDELETEARRAY(pTriZ0array);
	SAFEDELETEARRAY(pTriX1array);
	SAFEDELETEARRAY(pTriY1array);
	SAFEDELETEARRAY(pTriZ1array);
	SAFEDELETEARRAY(pTriX2array);
	SAFEDELETEARRAY(pTriY2array);
	SAFEDELETEARRAY(pTriZ2array);

	cudaFree(d_T0x);
	cudaFree(d_T0y);
	cudaFree(d_T0z);

	cudaFree(d_T1x);
	cudaFree(d_T1y);
	cudaFree(d_T1z);

	cudaFree(d_T2x);
	cudaFree(d_T2y);
	cudaFree(d_T2z);
	
	


	
}

extern "C" __declspec(dllexport) void reductionSDF_DLL(char * filepath, char * outfilepath, float * qX, float * qY, float * qZ, 	
	float * outDistance,
	unsigned long long  width, unsigned long long length, unsigned long long height)
{
	reductionSDF(filepath, outfilepath, qX, qY, qZ, outDistance, width, length,  height);
}

void generate2DRegularQueries(unsigned int resolution0, unsigned int resolution1, tjtypes::fvec3 center, char axis,  const char * exported_file_path)
{
	unsigned int num_queries = resolution0 * resolution1;

	float del0, del1;
	int i, j;
	del0 = 0.75f/ (float) resolution0;
	del1 = 0.75f/ (float) resolution1;

	float * pSlot0 = new float[resolution0];
	float * pSlot1 = new float[resolution1];

	for(i = 0; i < resolution0 ; i++)
		pSlot0[i] = (float) i * del0;

	for(i = 0; i < resolution1 ; i++)
		pSlot1[i] = (float) i * del1;

	FILE * fp = fopen(exported_file_path, "w");
	fprintf(fp, "%u\n", num_queries);

	if(axis == 0) // YZ plane
	{
		for(j = 0; j< resolution1 ; j++)
			for(i = 0; i < resolution0 ; i++)
				fprintf(fp, "%f\t%f\t%f\n", center.x, pSlot0[i], pSlot1[j]);
	}else if(axis == 1) // ZX plane
	{
		for(j = 0; j< resolution1 ; j++)
			for(i = 0; i < resolution0 ; i++)
				fprintf(fp, "%f\t%f\t%f\n", pSlot1[j], center.y, pSlot0[i]);
	}else if(axis == 2) // XY plane
	{
		for(j = 0; j< resolution1 ; j++)
			for(i = 0; i < resolution0 ; i++)
				fprintf(fp, "%f\t%f\t%f\n", pSlot0[i], pSlot1[j], center.z);
	}else
		printf("\nError. Wrong axis number.");

	fclose(fp);

}
int main(int argc, char* argv[])
{
	CStopWatch timer;
	timer.Start();
	float * qX = NULL;
	float * qY= NULL;
	float * qZ= NULL;
	std::string git_dir(getenv("GITDIR"));
//	std::string query_path = git_dir + "/mesh/query_points00.txt";
//	std::string query_path = git_dir + "/mesh/Query_samples2.txt";
	std::string query_path = git_dir + "/mesh/query_2d_256x256.txt";
	std::string mesh_path = git_dir + "/mesh/geosphere.off";
	std::string result_path = git_dir + "/mesh/reduction/result_geosphere.txt";

	tjtypes::fvec3 center = tjtypes::make_fvec3(0.75f/2.0f, 0.75f/2.0f, 0.75f/2.0f);

	generate2DRegularQueries(256,256, center, 0, query_path.c_str());

	unsigned int num_queries = getNumQueries(query_path.c_str());

	qX = new float[num_queries];
	qY = new float[num_queries];
	qZ = new float[num_queries];

	checkAndLoadQueries(query_path.c_str(), qX, qY, qZ, false);
	

	float *  outDistance = new float[num_queries];

	reductionSDF(mesh_path.c_str(), result_path.c_str() , qX, qY, qZ, 	outDistance,	num_queries, 1, 1);
	
	FILE * fp=fopen(result_path.c_str() , "w");
	fprintf(fp, "%d\n",num_queries);
	for(int i = 0 ; i < num_queries ; i++)
		fprintf(fp, "%f\n",outDistance[i]);

	fclose(fp);

	timer.Stop();
	timer.PrintAccumTime();

	SAFEDELETEARRAY(qX);
	SAFEDELETEARRAY(qY);
	SAFEDELETEARRAY(qZ);
	SAFEDELETEARRAY(outDistance);


	return 0;
}