#ifndef ____HS____TRIANGLE____
#define ____HS____TRIANGLE____

#include "Vertex.h"

struct gpuTriangle
{
	unsigned int		m_ui[3];
	gpuVertex			m_normals;
	gpuVertex			m_v[3];
};

class Triangle
{
public:

	unsigned int		m_Tid;
	unsigned int		m_ui[3];
	
	Vertex				m_normals;
	char				m_flag;
	
	Vertex				m_e[3];
	//unsigned int		m_uiNidx[3];

	//__host__ __global__ Triangle();
	//__host__ __global__ ~Triangle();

	__host__ __device__ Vertex * v(unsigned int index, Vertex * verts);
};

#endif