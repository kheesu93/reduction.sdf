#ifndef ____HS____MESH____
#define ____HS____MESH____

#include <string>
#include <iostream>
#include "Vertex.h"
#include "Triangle.h"
#include "HS_common.h"
#pragma warning(disable:4996)

using namespace std;

class Mesh
{
public:

	unsigned int		m_numVerts;
	unsigned int		m_numFaces;
	Vertex			*	m_pVerts;
	Triangle		*	m_pFaces;
	Vertex			*	m_pCentroid;
	unsigned int	*	m_pTidArray;

	unsigned int	*	m_pNeighFaces;
	unsigned int		m_NeighFcount;
	//unsigned int	*	m_NeighFidx;

	gpuVertex		*	m_gpuVerts;
	gpuTriangle		*	m_gpuFaces;

	vector<unsigned int> m_vNeighFaces;

	Mesh();
	~Mesh();

	void meshRead(std::string a);
	Vertex * getTcentroid(unsigned int * Tid);
	Vertex  VertexAdjacency(Vertex * pVert);
	void	ConvertTo075Canonical();
};

#endif