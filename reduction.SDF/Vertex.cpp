#include "Vertex.h"
#include <iostream>
/*
__device__ Vertex::Vertex()
{
	//m_vpFaces;
}

__host__ __device__ Vertex::~Vertex()
{
	//SAFEDELETEARRAY(vNfid);
}
*/

void Vertex::AddNeighbor(Triangle * pTris)
{
	//cout << "IN" << endl;
	m_vpFaces.push_back(pTris);
	//cout << "OUT" << endl;
}

int Vertex::NbPolygonNeighbor()
{
	return m_vpFaces.size();
}

Triangle * Vertex::GetPolygonNeighbor(int i)
{
	return m_vpFaces.at(i);
}