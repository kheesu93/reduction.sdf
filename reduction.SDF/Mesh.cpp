#include "Mesh.h"
#include "tjvector_types.h"
Mesh::Mesh()
{
	m_pVerts = NULL;
	m_pFaces = NULL;
	m_pCentroid = NULL;
	m_pTidArray = NULL;
	//m_pNeighFaces = NULL;
}

Mesh::~Mesh()
{
	SAFEDELETEARRAY(m_pVerts);
	SAFEDELETEARRAY(m_pFaces);
	SAFEDELETEARRAY(m_pCentroid);
	SAFEDELETEARRAY(m_pTidArray);
	//SAFEDELETEARRAY(m_pNeighFaces);
}

void Mesh::ConvertTo075Canonical()
{
	tjtypes::fvec3 v3Max = tjtypes::make_fvec3(-1000000.0f,-1000000.0f,-1000000.0f);
	tjtypes::fvec3 v3Min = tjtypes::make_fvec3(1000000.0f,1000000.0f,1000000.0f);

	tjtypes::fvec3 v3Centroid;
	tjtypes::fvec3 v3Length;
	
	float lmax;
	int numVerts = m_numVerts;
	Vertex v;
	for(int i = 0; i < numVerts ; i++)
	{
		v = m_pVerts[i];
		float x = v.x;
		float y = v.y;
		float z = v.z;

		if(x > v3Max.x)
			v3Max.x = x;
		if(y > v3Max.y)
			v3Max.y = y;
		if(z > v3Max.z)
			v3Max.z = z;

		if(x < v3Min.x)
			v3Min.x = x;
		if(y < v3Min.y)
			v3Min.y = y;
		if(z < v3Min.z)
			v3Min.z = z;

	}

	v3Centroid = tjtypes::make_fvec3(0.5f*(v3Max.x+v3Min.x), 0.5f*(v3Max.y+v3Min.y), 0.5f*(v3Max.z+v3Min.z));
	v3Length= tjtypes::make_fvec3(v3Max.x-v3Min.x, v3Max.y-v3Min.y, v3Max.z-v3Min.z);
	lmax = max( v3Length.x, max(v3Length.y, v3Length.z) );

	printf("\n (before conv) Min = (%f, %f, %f) ~ Max = (%f, %f, %f)\n",v3Min.x, v3Min.y, v3Min.z, v3Max.x, v3Max.y, v3Max.z);
	v3Max = tjtypes::make_fvec3(-1000000.0f,-1000000.0f,-1000000.0f);
	v3Min = tjtypes::make_fvec3(1000000.0f,1000000.0f,1000000.0f);

	for(int i = 0; i < numVerts ; i++)
	{
		v = m_pVerts[i];
		float x = v.x;
		float y = v.y;
		float z = v.z;

		m_pVerts[i].x = 0.75*(x-v3Centroid.x)/lmax + 0.75f/2.0f;
		m_pVerts[i].y = 0.75*(y-v3Centroid.y)/lmax + 0.75f/2.0f;
		m_pVerts[i].z = 0.75*(z-v3Centroid.z)/lmax + 0.75f/2.0f;


		x = m_pVerts[i].x;
		y = m_pVerts[i].y;
		z = m_pVerts[i].z;

		if(x > v3Max.x)
			v3Max.x = x;
		if(y > v3Max.y)
			v3Max.y = y;
		if(z > v3Max.z)
			v3Max.z = z;

		if(x < v3Min.x)
			v3Min.x = x;
		if(y < v3Min.y)
			v3Min.y = y;
		if(z < v3Min.z)
			v3Min.z = z;

	}
		printf("\n (after conv) Min = (%f, %f, %f) ~ Max = (%f, %f, %f)\n",v3Min.x, v3Min.y, v3Min.z, v3Max.x, v3Max.y, v3Max.z);


}
void Mesh::meshRead(std::string a)
{
	char myString[100];
	unsigned int dummy;

	FILE * fp = fopen(a.c_str(),"r");

	if(fp == NULL) perror("Error Opening File");
	else{
		fgets(myString, sizeof(myString), fp);
		cout << myString << endl;
		
		fscanf(fp, "%d %d %d\n", &m_numVerts, &m_numFaces, &dummy);
		printf("%d %d %d\n", m_numVerts, m_numFaces, dummy);

		m_pVerts = new Vertex[m_numVerts];
		m_pFaces = new Triangle[m_numFaces];
		
		m_gpuVerts = (gpuVertex *)malloc(m_numVerts * sizeof(gpuVertex));
		m_gpuFaces = (gpuTriangle *)malloc(m_numFaces * sizeof(gpuTriangle));

		for (unsigned int i = 0; i < m_numVerts; i++)
		{
			fscanf(fp, "%f %f %f\n", &m_pVerts[i].x, &m_pVerts[i].y, &m_pVerts[i].z);
			//cout << "  " << m_pVerts[i].x << "  "  << m_pVerts[i].y << "  "  << m_pVerts[i].z << endl;
			m_gpuVerts[i].x = m_pVerts[i].x;
			m_gpuVerts[i].y = m_pVerts[i].y;
			m_gpuVerts[i].z = m_pVerts[i].z;

		}

		for (unsigned int i = 0; i < m_numFaces; i++)
		{
			fscanf(fp, "%d %d %d %d", &dummy, &m_pFaces[i].m_ui[0], &m_pFaces[i].m_ui[1], &m_pFaces[i].m_ui[2]);
			//cout << dummy << "  " << m_pFaces[i].m_ui[0] <<"  " << m_pFaces[i].m_ui[1] <<"  " << m_pFaces[i].m_ui[2] << endl;
			m_gpuFaces[i].m_ui[0] = m_pFaces[i].m_ui[0];
			m_gpuFaces[i].m_ui[1] = m_pFaces[i].m_ui[1];
			m_gpuFaces[i].m_ui[2] = m_pFaces[i].m_ui[2];
		}

		fclose(fp);
	}

	for(unsigned int tid=0; tid<m_numFaces; tid++)
	{
		for(int j=0; j<3; j++)
		{
			m_gpuFaces[tid].m_v[j].x = m_pVerts[m_pFaces[tid].m_ui[j]].x;
			m_gpuFaces[tid].m_v[j].y = m_pVerts[m_pFaces[tid].m_ui[j]].y;
			m_gpuFaces[tid].m_v[j].z = m_pVerts[m_pFaces[tid].m_ui[j]].z;
		}
	}

	m_pTidArray = new unsigned int[m_numFaces];

	for(int i=0; i<m_numFaces; i++)
	{
		Triangle * pTris = &m_pFaces[i];
		pTris->m_Tid = i;
		m_pTidArray[i] = m_pFaces[i].m_Tid;
		
		for(int k=0; k<3; k++)
			pTris->v(k, m_pVerts)->AddNeighbor(pTris);
	
	}	
	
	getTcentroid(m_pTidArray);


	/////////// NeighborFace를 <vector>type으로 저장
	for(int j=0; j<m_numVerts; j++)
	{
		int numNb = m_pVerts[j].NbPolygonNeighbor();
		for(int k=0; k<numNb; k++)
			m_vNeighFaces.push_back(m_pVerts[j].GetPolygonNeighbor(k)->m_Tid);
	} 


	/////////// <vector>type의 NeighborFace를 --> array로 다시 저장!
	m_NeighFcount = m_vNeighFaces.size();
	m_pNeighFaces = new unsigned int[m_NeighFcount];
	for(int i=0; i<m_NeighFcount; i++)
		m_pNeighFaces[i] = m_vNeighFaces.at(i);


	/////////// 각 Neighbor Array의 시작하는 위치 인덱스 저장
	m_pVerts[0].vNfid = 0;
	for(int i=1; i<m_numVerts; i++)
	{
		m_pVerts[i].vNfid = m_pVerts[i-1].vNfid + m_pVerts[i-1].NbPolygonNeighbor();
		m_gpuVerts[i-1].gpuNfid = m_pVerts[i-1].vNfid;
	}
}

Vertex * Mesh::getTcentroid(unsigned int *  Tid)
{
	m_pCentroid = new Vertex[m_numFaces];
	
	for(int i=0; i<m_numFaces; i++)
	{
		unsigned int id = Tid[i];
		m_pCentroid[id].x = 0.0f;
		m_pCentroid[id].y = 0.0f;
		m_pCentroid[id].z = 0.0f;

		for(int j=0; j<3; j++)
		{
			m_pCentroid[id].x += m_pVerts[m_pFaces[id].m_ui[j]].x;
			m_pCentroid[id].y += m_pVerts[m_pFaces[id].m_ui[j]].y;
			m_pCentroid[id].z += m_pVerts[m_pFaces[id].m_ui[j]].z;
		}
		m_pCentroid[id].x /= 3.0f;
		m_pCentroid[id].y /= 3.0f;
		m_pCentroid[id].z /= 3.0f;
	}
	return m_pCentroid;
}

Vertex  Mesh:: VertexAdjacency(Vertex * pVert)
{
	float Nx=0;
	float Ny=0;
	float Nz=0;

	Vertex v3VertNormal;
	//Vertex * pVert = t.v(ui, m_pVerts);
	
	//int NbPolygonNeighbor = pVert->NbPolygonNeighbor();
	int NbPolygonNeighbor = pVert->NbPolygonNeighbor();
	//printf(">>>>>>>>>>>>>> %d \n", NbPolygonNeighbor);

	for(int k=0; k<NbPolygonNeighbor; k++)
	{
		Triangle * pNFaces = pVert->GetPolygonNeighbor(k);
		Nx += pNFaces->m_normals.x;
		Ny += pNFaces->m_normals.y;
		Nz += pNFaces->m_normals.z;
	}

	///////// A Triangle's one vertex normal x,y,z of three vertices
	pVert->nx = Nx /(float)NbPolygonNeighbor;
	pVert->ny = Ny /(float)NbPolygonNeighbor;
	pVert->nz = Nz /(float)NbPolygonNeighbor;

	v3VertNormal.x = pVert->nx;
	v3VertNormal.y = pVert->ny;
	v3VertNormal.z = pVert->nz;

	return v3VertNormal;
}
