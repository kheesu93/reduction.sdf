//****************************************************************************************
// Taejung Park (taejung.park@gmail.com)
// Created : 2010. 01. 09.
// Modified :
// SDFCUDA.h
// Contains declarations of wrap functions
//****************************************************************************************

#ifndef ___SDFCUDA__H____
#define ___SDFCUDA__H____
#include "commonTJ.h"


extern "C" bool InitCUDA(void);
extern "C" bool LoadTriangleData(REAL * pX0, REAL * pY0, REAL * pZ0,
								 REAL * pX1, REAL * pY1, REAL * pZ1, 
								 REAL * pX2, REAL * pY2, REAL * pZ2, 
								 UINTEG numFace);
extern "C" bool ReleaseTriangleData(void);
extern "C" bool CalculateSquaredDistance(REAL x, REAL y, REAL z, UINTEG numFace, UINTEG numFace2, UINTEG threads, UINTEG blocks);
extern "C" SHAREDATA GetNearestFacetidAndSqDist(REAL x, REAL y, REAL z, UINTEG numFace, UINTEG numFace2, UINTEG threads, UINTEG blocks);

#endif
