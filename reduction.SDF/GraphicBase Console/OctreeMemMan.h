#ifndef _OCTREEMEMMAN___
#define _OCTREEMEMMAN___

#include "CExplicitNodeInfo.h"

class OctreeMemMan
{
	CExplicitNodeInfo * m_pNodes;
	unsigned int m_begin, m_end, m_size;

public:
	CExplicitNodeInfo 		QuePop(void);
	void					QuePush(CExplicitNodeInfo Node);
	void					AllocNodes(int size);
	bool					IsEmpty(void);	
	unsigned int			GetPoppedIndex(void);
	unsigned int			GetPushedIndex(void);
	CExplicitNodeInfo 		GetANode(unsigned int index);
	void					RecordDensity(short int density);


	OctreeMemMan()
	{
		m_pNodes = NULL;
		m_begin = m_end = m_size =0;
	}

	OctreeMemMan(int size)
	{
		m_begin = m_end = m_size =0;
		AllocNodes(size);
	}
	~OctreeMemMan()
	{
		if(m_pNodes != NULL)
			delete m_pNodes;
	}

};

#endif
