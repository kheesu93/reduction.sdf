
#include "Transform.h"


CTransform::CTransform()
{
	 m_Scale = new CVector3d();
     m_Rotation = new CVector3d();
     m_Translation = new CVector3d();
     m_ValueRotation = 0;
}
CTransform::~CTransform() { Clear();}

// Data access
void CTransform::Clear(void)
{
//	delete m_Scale;
//	delete m_Rotation;
//	delete m_Translation;
}
CVector3d *CTransform::GetScale() { return m_Scale; }
CVector3d *CTransform::GetRotation() { return m_Rotation; }
CVector3d *CTransform::GetTranslation() { return m_Translation; }
float CTransform::GetValueRotation() { return m_ValueRotation; }

// Data setting
void CTransform::SetScale(CVector3d &vector) { *m_Scale = vector; }
void CTransform::SetRotation(CVector3d &vector) { *m_Rotation = vector; }
void CTransform::SetTranslation(CVector3d &vector) { *m_Translation = vector; }
void CTransform::SetValueRotation(float value) { m_ValueRotation = value; }

void CTransform::SetNewScale(CVector3d *pVector)
{ 
	double factor = pVector->Getx();

	if(m_Scale->Getx() != 0)
		*m_Scale *= factor;
 }

void CTransform::SetScale(CVector3d *pVector) { m_Scale = pVector; }
void CTransform::SetRotation(CVector3d *pVector) { m_Rotation = pVector; }
void CTransform::SetTranslation(CVector3d *pVector) { m_Translation = pVector; }

void CTransform::Copy(CTransform &transform)
{
	m_Scale = transform.GetScale();
	m_Rotation = transform.GetRotation();
	m_Translation = transform.GetTranslation();
	m_ValueRotation = transform.GetValueRotation();
}
void CTransform::Copy(CTransform *pTransform)
{
	m_Scale = pTransform->GetScale();
	m_Rotation = pTransform->GetRotation();
	m_Translation = pTransform->GetTranslation();
	m_ValueRotation = pTransform->GetValueRotation();
}