//********************************************
// SceneGraph.cpp
//********************************************
// class CSceneGraph
//********************************************



#include "Base3d.h"
#include "SceneGraph3d.h"
#include "PolygonMesh3d.h"


#define TRANS_POS 10000.0



//////////////////////////////////////////////
// CONSTRUCTORS
//////////////////////////////////////////////

//********************************************
// Constructor
//********************************************
CSceneGraph3d::CSceneGraph3d()
{
	m_ListDone = 0;

	m_range_checked = FALSE;
    m_lxrange = 0.0;
	m_lyrange = 0.0;
	m_lzrange = 0.0;
	m_pMesh = new CPolygonMesh3d;
}

//********************************************
// Destructor
//********************************************
CSceneGraph3d::~CSceneGraph3d()
{
	Free();
}

//********************************************
// Destructor
//********************************************
void CSceneGraph3d::Free(void)
{
	delete m_pMesh;		
	// Free objects
	CTArray<CObject3d>::Free();
}

//////////////////////////////////////////////
// OPENGL
//////////////////////////////////////////////

//********************************************
// BuildList
//********************************************


//********************************************
// glDraw
//********************************************

// ** EOF **







void CSceneGraph3d::CheckRange()
{

}

void CSceneGraph3d::ModelIntegrityCheck()
{
		CPolygonMesh3d *pPolygonMesh = NULL;
		CObject3d *pObject = GetAt(0);
 
		

		switch(pObject->GetType())
		{
			case TYPE_POLYGON_MESH3D: // triangle
				{
					pPolygonMesh = (CPolygonMesh3d *)pObject;
					for(long i =0; i < pPolygonMesh->GetPolygons()->GetSize();i++)
					{

					}
				}
		}

}

int CSceneGraph3d::SaveFile(char *name)
{
	/*
	if(NbObject() == 0)
	{
		AfxMessageBox("This scene does not contain meshes");
		return 0;
	}
	
	// Check for valid file
	CStdioFile file;
	CFileException ex;
	
	// Write header
	if(!WriteHeader(file,name))
	{
		AfxMessageBox("Error during writing header");
		return 0;
	}
	

	// Meshes
	for(int i=0;i<NbObject();i++)
	{
		CObject3d *pObject = GetAt(i);
		if(pObject->GetType() == TYPE_POLYGON_MESH3D)
			((CPolygonMesh3d *)pObject)->WriteFile(file);
	}
	
	// Close file
	file.Close();
	*/
	return 1;
}



float CSceneGraph3d::TranslateNumber(double number)
{
	float tmp = (float)(number*TRANS_POS);
	return (float)(tmp/TRANS_POS);
}



void CSceneGraph3d::ExtractionSDF(void)
{
	CObject3d *pObject3d = GetAt(0);
	CPolygonMesh3d *pMesh = (CPolygonMesh3d *)pObject3d;


//	pMesh->m_pScalarImage->InitializeSDF(pMesh, CVector3d(m_gxmin,m_gymin,m_gzmin), CVector3d(m_gxmax,m_gymax,m_gzmax));
//	pMesh->m_pScalarImage->BuildSDF();
}
