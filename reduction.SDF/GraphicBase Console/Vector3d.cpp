
#include "Vector3d.h"

CVector3d::CVector3d() 
{ 
	m_vx =0.0f; 
	m_vy =0.0f; 
	m_vz =0.0f; 
} 
CVector3d::~CVector3d(){}

// Constructors
CVector3d::CVector3d(const double x,const double y,const double z)
{
	m_vx = x; m_vy = y; m_vz = z;
}

CVector3d::CVector3d(CVector3d &vector)
{
	m_vx = vector.m_vx; 
	m_vy = vector.m_vy; 
	m_vz = vector.m_vz;
}

CVector3d::CVector3d(CVector3d *pVector)
{
	m_vx = pVector->m_vx; 
	m_vy = pVector->m_vy; 
	m_vz = pVector->m_vz;
}

CVector3d::CVector3d(CVector3d *pVertex2,CVector3d *pVertex1)
{
	m_vx = pVertex2->m_vx - pVertex1->m_vx;
	m_vy = pVertex2->m_vy - pVertex1->m_vy;
	m_vz = pVertex2->m_vz - pVertex1->m_vz;
}

CVector3d::CVector3d(CVector3d &vertex2,CVector3d &vertex1)
{
	m_vx = vertex2.m_vx - vertex1.m_vx;
	m_vy = vertex2.m_vy - vertex1.m_vy;
	m_vz = vertex2.m_vz - vertex1.m_vz;	
}

void CVector3d::Set(CVector3d *pVector)
{
	m_vx = pVector->m_vx; 
	m_vy = pVector->m_vy; 
	m_vz = pVector->m_vz;
}

void CVector3d::Set(CVector3d &vector)
{
	m_vx = vector.m_vx; 
	m_vy = vector.m_vy; 
	m_vz = vector.m_vz;
}

void CVector3d::Set(const double x,const double y,const double z)
{
	m_vx = x; m_vy = y; m_vz = z;
}

void CVector3d::Set(int n, double v)
{
	if(n==0)		
		 m_vx = v;
	else if(n==1)	
		m_vy = v;
	else if(n==2)	
		m_vz = v;
}



//********************************************
// Anglebetween
// by Haeyoung Lee
//********************************************
  
double CVector3d::Anglebetween(CVector3d *pVector, CVector3d *pNormal, double zero)
{

	double dot, dot2, cross, angle;
	CVector3d tmp;
    // find angle between this and pVector having pNormal as the perpendicular axis 
	//pFace and pF with normals
                //theta = tan1.Dot(tan0);
 	dot = Dot(pVector);
	tmp.Set(Cross(pVector));
	
	dot2 = tmp.Dot(pNormal);
	if ( dot2>= zero || fabs(dot2) < zero)
		cross = tmp.Length();
	else
        cross = -tmp.Length();
                 
	angle = atan2(cross, dot); 
	
	return angle;
}


double CVector3d::AngleBetweenVectors(CVector3d *pVector)
{
	
	double dot, cross;

	CVector3d tmp;
 	dot = Dot(pVector);  // Cos * norms
	tmp.Set(Cross(pVector));
	cross = tmp.Length(); //  Sin * norms
	return atan2(cross, dot);
}

//********************************************
// GetNormSquare
//********************************************
double CVector3d::GetNormL2Square(void)
{
	return (m_vx*m_vx + m_vy*m_vy + m_vz*m_vz);
}

void CVector3d::Copy(CVector3d &vector) { Set(vector); }
void CVector3d::Copy(CVector3d *pVector) { Set(pVector); }

void CVector3d::Setx(double x) {m_vx = x;}
void CVector3d::Sety(double y) {m_vy = y;} 
void CVector3d::Setz(double z) {m_vz = z;}

double CVector3d::Getx(void) { return m_vx; }
double CVector3d::Gety(void) { return m_vy; }
double CVector3d::Getz(void) { return m_vz; }

double CVector3d::Get(int n)
{
	if(n==0)		
		return m_vx;
	else if(n==1)	
		return m_vy;
	else if(n==2)	
		return m_vz;
	else 
		return 0.0;
}

CVector3d CVector3d::operator=(CVector3d& vector) {	Set(vector); return *this; }
CVector3d CVector3d::operator=(CVector3d* vector) {	Set(vector); return *this; }
void CVector3d::operator+=(CVector3d* pVector)
{
	m_vx += pVector->m_vx;
	m_vy += pVector->m_vy;
	m_vz += pVector->m_vz;
}

void CVector3d::operator+=(CVector3d& pVector)
{
	m_vx += pVector.m_vx;
	m_vy += pVector.m_vy;
	m_vz += pVector.m_vz;
}

void CVector3d::operator/=(double factor)
{
	m_vx = (double)m_vx/factor;
	m_vy = (double)m_vy/factor;
	m_vz = (double)m_vz/factor;
}

void CVector3d::operator/=(CVector3d &v)
{
	m_vx = (double)m_vx/v.m_vx;
	m_vy = (double)m_vy/v.m_vy;
	m_vz = (double)m_vz/v.m_vz;
}

void CVector3d::operator*=(double factor)
{
	m_vx = (double)m_vx*factor;
	m_vy = (double)m_vy*factor;
	m_vz = (double)m_vz*factor;
}

void CVector3d::operator+=(double factor)
{
	m_vx = (double)m_vx+factor;
	m_vy = (double)m_vy+factor;
	m_vz = (double)m_vz+factor;
}

void CVector3d::operator-=(double factor)
{
	m_vx = (double)m_vx-factor;
	m_vy = (double)m_vy-factor;
	m_vz = (double)m_vz-factor;
}

CVector3d& CVector3d::operator-=(CVector3d& rVector)
{
	m_vx -= rVector.m_vx;
	m_vy -= rVector.m_vy;
	m_vz -= rVector.m_vz;

	return *this;
}

CVector3d& CVector3d::operator-=(CVector3d* pVector)
{
	m_vx -= pVector->m_vx;
	m_vy -= pVector->m_vy;
	m_vz -= pVector->m_vz;

	return *this;
}

/*
void CVector3d::CrossProduct(CVector3d& vector)
{
	
}
*/

CVector3d CVector3d::CrossProduct(CVector3d v1,CVector3d v2)
{
	return CVector3d(-v1.z() * v2.y() + v1.y() * v2.z(), v1.z() * v2.x() - v1.x() * v2.z(), -v1.y() * v2.x() + v1.x() * v2.y());	
}


double CVector3d::Dot(CVector3d& rVector)
{
	return (m_vx*rVector.m_vx + m_vy*rVector.m_vy + m_vz*rVector.m_vz);
}

double CVector3d::Dot(CVector3d* pVector)
{
	return (m_vx*pVector->m_vx + m_vy*pVector->m_vy + m_vz*pVector->m_vz);
}


//***************************************
// ProcessInner
//***************************************
void CVector3d::ProcessInner(CVector3d* u,
							 CVector3d* v) 
{
	m_vx = u->m_vy*v->m_vz-u->m_vz*v->m_vy;
	m_vy = u->m_vz*v->m_vx-u->m_vx*v->m_vz;
	m_vz = u->m_vx*v->m_vy-u->m_vy*v->m_vx;
	
}

CVector3d CVector3d::Cross(CVector3d& v)
{
	CVector3d tmpV;

	tmpV.Setx( (m_vy * v.m_vz) - (m_vz * v.m_vy) );
	tmpV.Sety( (m_vz * v.m_vx) - (m_vx * v.m_vz) );
	tmpV.Setz( (m_vx * v.m_vy) - (m_vy * v.m_vx) );

	return tmpV;
}
CVector3d CVector3d::Cross(CVector3d* pV)
{
	CVector3d tmpV;

	tmpV.m_vx =( (m_vy * pV->m_vz) - (m_vz * pV->m_vy) );
	tmpV.m_vy =( (m_vz * pV->m_vx) - (m_vx * pV->m_vz) );
	tmpV.m_vz =( (m_vx * pV->m_vy) - (m_vy * pV->m_vx) );

	return tmpV;
}

//void CVector3d::NormalizeL2(void);

double CVector3d::Length()
{
	return (double)(sqrt(LengthSquared()));
}

double CVector3d::LengthSquared()
{
	return (double)( m_vx*m_vx + m_vy*m_vy + m_vz*m_vz );
}

int CVector3d::Equals(CVector3d* v)
{
	if( m_vx == v->m_vx && m_vy == v->m_vy && m_vz == v->m_vz) return 1;
	else return 0;
}


// Found errors for vector addition and subtraction by TJ. The original one was as follows:
/*

CVector3d operator+(CVector3d& u,CVector3d& v)
{
	CVector3d vTmp;
	vTmp.Set(u.Getx()+v.Getx() , u.Gety()+v.Gety() , u.Gety()+v.Gety() );
	return vTmp;
}

CVector3d operator-(CVector3d& u,CVector3d& v)
{
	CVector3d vTmp;
	vTmp.Set(u.Getx()-v.Getx() , u.Gety()-v.Gety() , u.Gety()-v.Gety() );
	return vTmp;
}
*/
CVector3d operator+(CVector3d& u,CVector3d& v)
{
	CVector3d vTmp;
	vTmp.Set(u.m_vx+v.m_vx , u.m_vy+v.m_vy , u.m_vz+v.m_vz );
	return vTmp;
}

CVector3d operator-(CVector3d& u,CVector3d& v)
{
	CVector3d vTmp;
	vTmp.Set(u.m_vx-v.m_vx , u.m_vy-v.m_vy , u.m_vz-v.m_vz );
	return vTmp;
}

CVector3d operator^(CVector3d &u,CVector3d &v)
{
	//Cross product
	return u.Cross(v);
}

CVector3d CVector3d::operator/(const double & f)	
{
	return CVector3d(this->m_vx/f, this->m_vy/f, this->m_vz/f);
}

CVector3d CVector3d::operator*(const double & f)	
{
	return CVector3d(this->m_vx*f, this->m_vy*f, this->m_vz*f);
}

CVector3d CVector3d::operator*(CVector3d & v)	
{
	return CVector3d(this->m_vx*v.m_vx, this->m_vy*v.m_vy, this->m_vz*v.m_vz);
}


//********************************************
// Inner
//********************************************
CVector3d CVector3d::Inner(CVector3d* u,
                           CVector3d* v)

{

	// w = u ^ v
	CVector3d w;
	w.Set(u->m_vy*v->m_vz-u->m_vz*v->m_vy,
		u->m_vz*v->m_vx-u->m_vx*v->m_vz,
		u->m_vx*v->m_vy-u->m_vy*v->m_vx);
	return w;
}


//********************************************
// Normalize
//********************************************
void CVector3d::NormalizeL2(void)
{
	double norm = GetNormL2();
	if(norm != 0)
	{
		m_vx = m_vx / norm;
		m_vy = m_vy / norm;
		m_vz = m_vz / norm;
	}
	//TRACE("norm : %g\n",GetNormL2());
}

//********************************************
// Normalize
//********************************************
void CVector3d::NormalizeL2(double value)
{
	double norm = GetNormL2();
	if(norm != 0.0f)
	{
		m_vx *= (value/norm);
		m_vy *= (value/norm);
		m_vz *= (value/norm);
	}
	//TRACE("norm : %g (wanted : %g)\n",GetNormL2(),value);
}

//********************************************
// GetNorm
//********************************************
double CVector3d::GetNormL2(void)
{
	return sqrt(m_vx*m_vx + m_vy*m_vy + m_vz*m_vz);
}

//********************************************
// Projection
//********************************************
CVector3d CVector3d::Projection(CVector3d& v) 
{
	
	double alpha = Dot(v)/v.Dot(v);
	return CVector3d(m_vx-alpha*v.m_vx, m_vy-alpha*v.m_vy,
		     m_vz-alpha*v.m_vz);
}

//********************************************
// Projection
// by Haeyoung Lee
//********************************************
CVector3d   CVector3d::Projection(CVector3d* pV)

{

   double alpha = Dot(pV)/pV->Dot(pV);
	return CVector3d(m_vx - alpha* pV->m_vx, m_vy - alpha*pV->m_vy,
		     m_vz - alpha*pV->m_vz);
}

//********************************************
// Rotate this vector around pAround by angle
// by Haeyoung Lee
//********************************************

CVector3d CVector3d::Rotate(double angle, CVector3d& Around) 
{

   double f1, f2, f3, f4, f5, f6;
   CVector3d t1, t2;
 

   f1 = cos(angle);
   f2 = sin(angle);
   t1.Set(Projection(Around));
   t2.Set(Around.Cross(this));
   f3 = Dot(Around);
   f4 = (f1*t1.m_vx+f2*t2.m_vx+f3*Around.m_vx);
   f5 = (f1*t1.m_vy+f2*t2.m_vy+f3*Around.m_vy);
   f6 = (f1*t1.m_vz+f2*t2.m_vz+f3*Around.m_vz);
   return CVector3d(f4, f5, f6);
       
}


CVector3d CVector3d::Rotate(double angle, CVector3d* pAround) 
{
	
 double f1, f2, f3, f4, f5, f6;
 CVector3d t1, t2;
 
   f1 = cos(angle);
   f2 = sin(angle);
   t1.Set(Projection(pAround));
   t2.Set(pAround->Cross(this));
   f3 = Dot(pAround);
   f4 = f1*t1.m_vx+f2*t2.m_vx+f3*pAround->m_vx;
   f5 = f1*t1.m_vy+f2*t2.m_vy+f3*pAround->m_vy;
   f6 = f1*t1.m_vz+f2*t2.m_vz+f3*pAround->m_vz;
   return CVector3d(f4,f5,f6);
}

void CVector3d::SetValue(int n, double d)
{
	if(n==0)		m_vx=d;
	else if(n==1)	m_vy=d;
	else if(n==2)	m_vz=d;
}

double CVector3d::GetValue(int n)
{
	if(n==0)		return m_vx;
	else if(n==1)	return m_vy;
	else if(n==2)	return m_vz;
	else return 0.0;
}