#include "stdafx.h"
#include "OctreeMemMan.h"

bool OctreeMemMan::IsEmpty()
{
	if(m_begin == m_end)
		return true;
	else 
		return false;
}

unsigned int OctreeMemMan::GetPoppedIndex(void)
{
	return m_begin;
}

unsigned int OctreeMemMan::GetPushedIndex(void)
{
	return m_end;
}

void OctreeMemMan::RecordDensity(short int density)
{
	m_pNodes[m_begin].m_iAVGDensity = density;
}


CExplicitNodeInfo OctreeMemMan::QuePop(void)
{
	if(m_begin  >= m_size)
		m_begin = 0;
	else
		m_begin++;
	
	return m_pNodes[m_begin];	
}

CExplicitNodeInfo OctreeMemMan::GetANode(unsigned int index)
{
//	ASSERT( m_begin <= index && m_end < index);
	return m_pNodes[index];
}

void OctreeMemMan::QuePush(CExplicitNodeInfo node)
{
	if(m_end  >= m_size)
		m_end = 0;
	else 
		m_end++;



	m_pNodes[m_end].m_iLevel = node.m_iLevel;	
	m_pNodes[m_end].m_imaxX = node.m_imaxX;	
	m_pNodes[m_end].m_imaxY = node.m_imaxY;	
	m_pNodes[m_end].m_imaxZ = node.m_imaxZ;	
	m_pNodes[m_end].m_iminX = node.m_iminX;	
	m_pNodes[m_end].m_iminY = node.m_iminY;	
	m_pNodes[m_end].m_iminZ = node.m_iminZ;	


}

void	OctreeMemMan::AllocNodes(int size)
{
	m_pNodes = new CExplicitNodeInfo[size];
	m_size = size;
}
