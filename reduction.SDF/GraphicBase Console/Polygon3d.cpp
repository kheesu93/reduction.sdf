//********************************************
// Polygon3d.cpp
//********************************************
// class CPolygon3d
//********************************************
// alliez@usc.edu
// Created : 15/01/98
// Modified : 06/03/01
//********************************************


#include "Base3d.h"
#include "Polygon3d.h"
#include <float.h>



//////////////////////////////////////////////
// CONSTRUCTORS
//////////////////////////////////////////////

//////////////////////////////////////////////
// DATAS
//////////////////////////////////////////////

//********************************************
// GetType
//********************************************
int CPolygon3d::GetType()
{
    return TYPE_MESH3D;
}

//********************************************
// IsValid
//********************************************
int CPolygon3d::IsValid()
{
	int size = GetSize();
	for(int i=0;i<(size-1);i++)
		for(int j=i+1;j<size;j++)
			if(m_Vertices[i] == m_Vertices[j])
				return 0;
  return 1;
}


//***************************************
// ComputeBarycenter
//***************************************
void CPolygon3d::ComputeBarycenter(double &x,
								 double &y, 
								 double &z)
{
	x = y = z = 0.0f;
	int d = m_Vertices.GetSize();
	for(int i=0;i<d;i++)
	{
		x += m_Vertices[i]->x(); 
		y += m_Vertices[i]->y(); 
		z += m_Vertices[i]->z(); 
	}
	x /= d;y /= d;z /= d;
	x = ceil(x*1000000.f)/1000000.f;
  y = ceil(y*1000000.f)/1000000.f;
	z = ceil(z*1000000.f)/1000000.f;
}


void CPolygon3d::SetNormal(double x,double y,double z)
	{ 
		m_Normal.Set(x,y,z);
	}


//***************************************
// CalculateNormal
//***************************************
void CPolygon3d::CalculateNormal()
{ 
	
	int d = m_Vertices.GetSize();
/*	if (d == 3)
	{
		 CVector3d Tmpv;

	     m_Normal.Set(m_Vertices[1]->x()-m_Vertices[0]->x(), 
			          m_Vertices[1]->y()-m_Vertices[0]->y(), 
					  m_Vertices[1]->z()-m_Vertices[0]->z());
	     Tmpv.Set(m_Vertices[2]->x()-m_Vertices[0]->x(), 
			          m_Vertices[2]->y()-m_Vertices[0]->y(), 
					  m_Vertices[2]->z()-m_Vertices[0]->z());

	     m_Normal.Set(m_Normal.Cross(&Tmpv));

	}
	else
	{
*/		double xc,yc,zc;
		ComputeBarycenter(xc,yc,zc);

		double nx = 0.0f;
		double ny = 0.0f;
		double nz = 0.0f;

		CVector3d n;
		for(int i=0;i<d;i++)
		{
			CVector3d v1(m_Vertices[i]->x()-xc,
						m_Vertices[i]->y()-yc,
						m_Vertices[i]->z()-zc);
			v1.Set(ceil(v1.x()*1000000.f)/1000000.f, 
					 ceil(v1.y()*1000000.f)/1000000.f,
						 ceil(v1.z()*1000000.f)/1000000.f);

			CVector3d v2(m_Vertices[(i+1)%d]->x()-xc,
						m_Vertices[(i+1)%d]->y()-yc,
						m_Vertices[(i+1)%d]->z()-zc);
		v2.Set(ceil(v2.x()*1000000.f)/1000000.f, 
					 ceil(v2.y()*1000000.f)/1000000.f,
						 ceil(v2.z()*1000000.f)/1000000.f);

			n.ProcessInner(&v1,&v2);
			n.NormalizeL2();
			n.Set(ceil(n.x()*1000000.f)/1000000.f, 
					ceil(n.y()*1000000.f)/1000000.f,
			  		ceil(n.z()*1000000.f)/1000000.f);

			nx += n.x();
			ny += n.y();
			nz += n.z();
    
		}

		// averaging
		m_Normal.Set(ceil((nx/d)*1000000.f)/1000000.f,ceil((ny/d)*1000000.f)/1000000.f,ceil((nz/d)*1000000.f)/1000000.f);
		//m_Normal.Set(nx/d, ny/d, nz/d);
		m_Normal.NormalizeL2();
		m_Normal.Set(ceil(m_Normal.x()*1000000.f)/1000000.f, 
						 ceil(m_Normal.y()*1000000.f)/1000000.f,
							 ceil(m_Normal.z()*1000000.f)/1000000.f);

//	}
}

void CPolygon3d::GetReCenter(void)
{
	CVector3d v0,v1,v2;
	v0.Set(this->v(0)->x(),this->v(0)->y(),this->v(0)->z());
	v1.Set(this->v(1)->x(),this->v(1)->y(),this->v(1)->z());
	v2.Set(this->v(2)->x(),this->v(2)->y(),this->v(2)->z());
	m_vCenter.x((v0.x()+v1.x()+v2.x())/3.0);
	m_vCenter.y((v0.y()+v1.y()+v2.y())/3.0);
	m_vCenter.z((v0.z()+v1.z()+v2.z())/3.0);
}

double	CPolygon3d::GetArea(void)  // added by TJ Park
{
	CVector3d s1;
	CVector3d s2;
	CVector3d cross;

	s1.x(v(1)->x() - v(0)->x());
	s1.y(v(1)->y() - v(0)->y());
	s1.z(v(1)->z() - v(0)->z());

	s2.x(v(2)->x() - v(0)->x());
	s2.y(v(2)->y() - v(0)->y());
	s2.z(v(2)->z() - v(0)->z());

	cross = s1 ^ s2;

	return 0.5*cross.Length();

}

void CPolygon3d::CalculateNormal2()
{
	CVector3d v0,v1,v2;
/*
	TRACE("\n(%f,%f,%f)",this->v(0)->x(),this->v(0)->y(),this->v(0)->z());
	TRACE("\n(%f,%f,%f)",this->v(1)->x(),this->v(1)->y(),this->v(1)->z());
	TRACE("\n(%f,%f,%f)",this->v(2)->x(),this->v(2)->y(),this->v(2)->z());
*/
	ASSERT(Degree() > 2);
	v0.Set(this->v(0)->x(),this->v(0)->y(),this->v(0)->z());
	v1.Set(this->v(1)->x(),this->v(1)->y(),this->v(1)->z());
	v2.Set(this->v(2)->x(),this->v(2)->y(),this->v(2)->z());


	CVector3d va, vb;

	va = v1-v0;
	vb = v2-v0;

	CVector3d Normal(va.Cross(vb));

	Normal = Normal / Normal.Length();

	m_Normal.Set(Normal.x(),Normal.y(), Normal.z());
}


// ** EOF **
