
#include "math.h"
#include "float.h"
#include "Base3d.h"
#include "PolygonVertex3d.h"
#include "Polygon3d.h"


//////////////////////////////////////////////
// DATAS
//////////////////////////////////////////////

//********************************************
// GetType
//********************************************
//int CPolygonVertex3d::GetType()
//{
//	return TYPE_POLYGON_VERTEX3D;
//}

//***************************************
// GetNbNeighborPolygonWithFlag
//***************************************
int CPolygonVertex3d::GetNbNeighborPolygonWithFlag(int flag)
{ 
	int NbPolygon = m_Polygons.GetSize();
	int nb = 0;
	for(int i=0;i<NbPolygon;i++)
		nb += (m_Polygons[i]->GetFlag() == flag);
	return nb;
}

//***************************************
// GetNbNeighborPolygonWithFlag
//***************************************
int CPolygonVertex3d::GetNbNeighborPolygonWithFlagWeighted(int flag)
{ 
	int NbPolygon = m_Polygons.GetSize();
	int nb = 0;
	for(int i=0;i<NbPolygon;i++)
	{
		CPolygon3d *pPolygon = m_Polygons[i];
		int degree = pPolygon->Degree();
		if(pPolygon->GetFlag() == flag)
			if(degree == 3)
				nb += 1;
			else
				nb += 1;
	}
	return nb;
}


//********************************************
// IsOnBoundary
//********************************************
int CPolygonVertex3d::IsOnBoundary()
{
	int size = m_Polygons.GetSize();
	for(int i=0;i<size;i++)
	{
		CPolygon3d *pPolygon = m_Polygons[i];
		int degree = pPolygon->Degree();
		int index = pPolygon->IndexFrom(this);
		if(pPolygon->f(index) == NULL ||
			 pPolygon->f((index+degree-1)%degree) == NULL)
			 return 1;
	}
	return 0;
}

//********************************************
// HasBothFlagsOnPolygons
//********************************************
int CPolygonVertex3d::HasBothFlagsOnPolygons(int flag1,
																						 int flag2)
{
	int has1 = 0;
	int has2 = 0;
	int size = m_Polygons.GetSize();
	for(int i=0;i<size;i++)
	{
		CPolygon3d *pPolygon = m_Polygons[i];
		if(pPolygon->GetFlag() == flag1)
			has1 = 1;
		else
			if(pPolygon->GetFlag() == flag2)
				has2 = 1;
	}
	return (has1 && has2);
}


//***************************************
// GetPolygonTwoVerticesOrder
//***************************************
CPolygon3d *CPolygonVertex3d::GetPolygonTwoVerticesOrder(CPolygonVertex3d *pV1,
														 CPolygonVertex3d *pV2) 
{
	int size = m_Polygons.GetSize();
	for(int i=0;i<size;i++)
	{
		CPolygon3d *pPolygon = m_Polygons[i];
		if(pPolygon->Has2VerticesOrder(pV1,pV2))
			return pPolygon;
	}
	return NULL;
}
CPolygon3d *CPolygonVertex3d::GetPolygonTwoVertices(CPolygonVertex3d *pV1,CPolygonVertex3d *pV2,
													CPolygon3d *pPoly)
{
	int size = m_Polygons.GetSize();
	for(int i=0;i<size;i++)
	{
		CPolygon3d *pPolygon = m_Polygons[i];
		if((pPolygon != pPoly) && (pPolygon->Has2Vertices(pV1, pV2)))
			return pPolygon;
	}
	return NULL;
}

int CPolygonVertex3d::RemoveANeighbor(CPolygon3d *pNeighbor)
{
	int size = m_Polygons.GetSize();
	int inx = -1;
	for (int i = 0; i<size; i++)
	{
		CPolygon3d *pPoly = m_Polygons.GetAt(i);
		if (pPoly == pNeighbor)
		{
           inx = i;
		   break;
		}
	}
	if (inx > -1)
    	m_Polygons.RemoveAt(inx);

	return inx;
}

CVector3d * CPolygonVertex3d::GetNormal(void)
{ 
	if(m_Normal.Getx() == 0 && m_Normal.Gety() == 0 && m_Normal.Getz() == 0)
		CalculateNormal();

	return &m_Normal;
}

void CPolygonVertex3d::CalculateNormal()
{
	double x=0, y=0, z=0 ;	
	int nb = NbPolygonNeighbor();

	for( int i=0 ; i< nb ; i++ )
	{
		x += GetPolygonNeighbor(i)->GetNormal()->Getx();
		y += GetPolygonNeighbor(i)->GetNormal()->Gety();
		z += GetPolygonNeighbor(i)->GetNormal()->Getz();
	}
	x/=(double)nb; y/=(double)nb; z/=(double)nb;
	m_Normal.Set(x,y,z);
}


CPolygon3d * CPolygonVertex3d::FindNextFace(CPolygon3d * pSeedFace,CPolygonVertex3d * pVertex,CPolygonVertex3d * pNextVertex)
{
	bool bFound = false;

	CPolygon3d * pTestFace;

	for(int i = 0 ; i < pSeedFace->NbPolygonNeighbor() ; i++)
	{
		pTestFace = pSeedFace->f(i);
		if(pTestFace->Has2Vertices(pVertex,pNextVertex) && pTestFace!= pSeedFace)
		{
			bFound = true;
			break;
		}
	}

	if(bFound)
		return pTestFace;
	else
		return NULL;	


}


// ** EOF **
