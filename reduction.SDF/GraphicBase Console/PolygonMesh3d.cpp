//********************************************
// QuadMesh3d.cpp
//********************************************
// class CPolygonMesh3d
//********************************************
// alliez@usc.edu
// Created  : 03/06/01
// Modified : 03/06/01
//********************************************


#include <math.h>
#include <float.h>
#include "Base3d.h"
#include "PolygonMesh3d.h"
#include "SceneGraph3d.h"



#define	DEBUGGING_COLOR	false


//////////////////////////////////////////////
// CONSTRUCTORS
//////////////////////////////////////////////

//********************************************
// Constructor
//********************************************
CPolygonMesh3d::CPolygonMesh3d()
{
	m_ListDone = 0;
	m_Modified = 1;
    m_forcedview = false;
//	m_pScalarImage = new CPrimalTree();
	m_nRenderingMode = 0;
}

//********************************************
// Destructor
//********************************************

CPolygonMesh3d::~CPolygonMesh3d()
{
	Free();
//	delete m_pScalarImage;
}

//********************************************
// Destructor
//********************************************
void CPolygonMesh3d::Free()
{
	m_Vertices.Free();
	m_Polygons.Free();
}
void CPolygonMesh3d::FreePolygonsOnly()
{
    m_Polygons.RemoveAll();
}
//////////////////////////////////////////////
// OPENGL
//////////////////////////////////////////////



//********************************************
// GetType
//********************************************
int CPolygonMesh3d::GetType()
{
	return TYPE_POLYGON_MESH3D;
}

//********************************************
// SetFlagOnPolygons
//********************************************
void CPolygonMesh3d::SetFlagOnPolygons(int flag)
{
	int size = m_Polygons.GetSize();
	for(int i=0;i<size;i++)
		m_Polygons[i]->SetFlag(flag);
}

//********************************************
// SetFlagOnVertices
//********************************************
void CPolygonMesh3d::SetFlagOnVertices(int flag)
{
	int size = m_Vertices.GetSize();
	for(int i=0;i<size;i++)
		m_Vertices[i]->SetFlag(flag);
}

//********************************************
// SetTagOnVertices
//********************************************
void CPolygonMesh3d::SetTagOnVertices(int tag)
{
	int size = m_Vertices.GetSize();
	for(int i=0;i<size;i++)
		m_Vertices[i]->SetTag(tag);
}

//********************************************
// SetColorPolygons
//********************************************
int CPolygonMesh3d::IsValid()
{
	int size = m_Polygons.GetSize();
	for(int i=0;i<size;i++)
		if(!m_Polygons[i]->IsValid())
			return 0;
	return 1;
}

//***************************************
// Rebuild
//***************************************
void CPolygonMesh3d::Rebuild()
{ 
	BuildAdjacency();
	ProcessNormalsPerFace();
}

void CPolygonMesh3d::SynthesisRange(void)
{
	int NbVertices = m_Vertices.GetSize()-1;
	double minx = 1000.0, miny = 1000.0, minz = 1000.0;
	double maxx =-1000.0, maxy =-1000.0, maxz =-1000.0;
	for(int i = 0; i < 8; i++) {
		CPolygonVertex3d *pVertex = m_Vertices[NbVertices-i];
		if(minx > pVertex->x())	minx = pVertex->x();
		if(miny > pVertex->y())	miny = pVertex->y();
		if(minz > pVertex->z())	minz = pVertex->z();
		if(maxx < pVertex->x())	maxx = pVertex->x();
		if(maxy < pVertex->y())	maxy = pVertex->y();
		if(maxz < pVertex->z())	maxz = pVertex->z();
		m_Vertices.RemoveAt(m_Vertices.IndexFrom(pVertex));
	}
	for(int i = 0; i <= NbVertices; i++) {
		CPolygonVertex3d *pVertex = m_Vertices[i];
		if(minx <= pVertex->x() && pVertex->x() <= maxx) {
			if(miny <= pVertex->y() && pVertex->y() <= maxy) {
				if(minz <= pVertex->z() && pVertex->z() <= maxz) {
					pVertex->m_Color.Set(1,0,0);
				}
			}
		}
	}
}

//***************************************
// BuildAdjacency
//***************************************
int CPolygonMesh3d::BuildAdjacency()
{ 
	int NbVertex = m_Vertices.GetSize();
	int NbPolygon = m_Polygons.GetSize();

	int i;
	// init faces
	for(i=0;i<NbPolygon;i++)
	{
		CPolygon3d *pPolygon = m_Polygons[i];
		int d = pPolygon->GetSize();
		for(int k=0;k<d;k++)
			pPolygon->f(k,NULL);
	}

	// init vertices	
  for(i=0;i<NbVertex;i++)
		m_Vertices[i]->RemoveAllNeighbors();

	// vertex/polygon
  for(i=0;i<NbPolygon;i++)
  {
		CPolygon3d *pPolygon = m_Polygons[i];
		int d = pPolygon->GetSize();
		for(int k=0;k<d;k++)
			pPolygon->v(k)->AddNeighbor(pPolygon);

  }
		
	// vertex/vertex
  for(i=0;i<NbVertex;i++)
  {
		CPolygonVertex3d *pVertex = m_Vertices[i];
		int NbPolygonNeighbor = pVertex->NbPolygonNeighbor();
		for(int j=0;j<NbPolygonNeighbor;j++)
		{
			CPolygon3d *pNPolygon = pVertex->GetPolygonNeighbor(j);
			int d = pNPolygon->GetSize();
			int index = pNPolygon->IndexFrom(pVertex);
			pVertex->AddNeighbor(pNPolygon->v((index+1)%d));
			pVertex->AddNeighbor(pNPolygon->v((index+d-1)%d));
    }
	}

	// face/faces	
	for(i=0;i<NbPolygon;i++)
	{
		CPolygon3d *pPolygon = m_Polygons[i];
		int d = pPolygon->GetSize();
		// For each edge
		for(int j=0;j<d;j++)
		{
			CPolygonVertex3d *pVertex = pPolygon->v(j);
			CPolygonVertex3d *pNextVertex = pPolygon->v((j+1)%d);
			int NbPolygonNeighbor = pVertex->NbPolygonNeighbor();
			for(int k=0;k<NbPolygonNeighbor;k++)
			{
				// This face contain pVertex
				CPolygon3d *pNPolygon = pVertex->GetPolygonNeighbor(k);
				if(pPolygon != pNPolygon)
					if(pPolygon->f(j) == NULL)
						if(pNPolygon->HasVertex(pVertex))
							if(pNPolygon->HasVertex(pNextVertex))
								pPolygon->f(j,pNPolygon);
			}
		}
	}
		
	return TRUE;
}

//********************************************
// Smooth
// 30/09/98
//********************************************
int CPolygonMesh3d::Smooth()
{
	// copy
	int NbVertex = m_Vertices.GetSize();
	float *pPos = new float[3*NbVertex];
	
	// for each vertex (at least 3 neighbors)
	int i;
	for(i=0;i<NbVertex;i++)
	{
		CPolygonVertex3d *pVertex = m_Vertices[i];
		int n = pVertex->NbVertexNeighbor();
		// for each coord
		for(unsigned int j=0;j<3;j++)
		{
			double value = pVertex->Get(j);
			for(int k=0;k<n;k++)
				value += pVertex->GetVertexNeighbor(k)->Get(j);
			pPos[3*i+j] = (float)value/(float)(n+1);
		}
	}
	
	// restore
	for(i=0;i<NbVertex;i++)
		m_Vertices[i]->Set(pPos[3*i],pPos[3*i+1],pPos[3*i+2]);
		
	// cleanup and rebuild
	delete [] pPos;
  Rebuild();
  m_Modified = 1;
  return 1;
}

//********************************************
// ProcessNormalsPerFace
//********************************************
int CPolygonMesh3d::ProcessNormalsPerFace(void)
{
	for(int i=0;i<m_Polygons.GetSize();i++)
		m_Polygons[i]->CalculateNormal();
	m_Modified = 1;
	return 1;
}

//////////////////////////////////////////////
// I/O
//////////////////////////////////////////////

//***************************************
// SaveFile
//***************************************
// NbVertex	int32
// NbPolygons	int32
// x1 y1 z1	3 x float32
// x2 y2 z2
// .. .. ..
// d v1 v2 v3 ... vd (0-based) d x int32 
// .. .. .. ..
//***************************************
void CPolygonMesh3d::SaveFile(char *filename)
{
	// open file
	FILE *pFile = fopen(filename,"wb");
	if(!pFile)
	{
		return;
	}
	int NbVertex = m_Vertices.GetSize();
	int NbPolygons = m_Polygons.GetSize();
	fwrite(&NbVertex,sizeof(32),1,pFile);
	fwrite(&NbPolygons,sizeof(32),1,pFile);

	// output vertices
	float coord[3];
	int i;
	for(i=0;i<NbVertex;i++)
	{
		coord[0] = (float)m_Vertices[i]->x();
		coord[1] = (float)m_Vertices[i]->y();
		coord[2] = (float)m_Vertices[i]->z();
		fwrite(coord,sizeof(float),3,pFile);
	}

	// store indices in flags, much faster
	for(i=0;i<NbVertex;i++)
		m_Vertices[i]->SetFlag(i);

	// output polygons
	for(i=0;i<NbPolygons;i++)
	{
		int d = m_Polygons[i]->GetSize();
		fwrite(&d,sizeof(int),1,pFile);
		for(int j=0;j<d;j++)
		{
			int index = m_Polygons[i]->v(j)->GetFlag();
			fwrite(&index,sizeof(int),1,pFile);
		}
	}
	fclose(pFile);
}

//***************************************
// ReadFile
//***************************************
void CPolygonMesh3d::ReadFile(char *filename)
{ 
	/*
	// open file
	FILE *pFile = fopen(filename,"rb");
	if(!pFile)
	{
		TRACE("** unable to open file %s for reading\n",filename);
		return;
	}
	int NbVertex,NbPolygons;
	fread(&NbVertex,sizeof(32),1,pFile);
	fread(&NbPolygons,sizeof(32),1,pFile);
	m_Vertices.SetSize(NbVertex);
	m_Polygons.SetSize(NbPolygons);

	// input vertices
	float coord[3];
	for(int i=0;i<NbVertex;i++)
	{
		fread(coord,sizeof(float),3,pFile);
		m_Vertices.SetAt(i,new CPolygonVertex3d(coord[0],coord[1],coord[2]));
	}

	// input polygons
	int indices[4];
	for(i=0;i<NbPolygons;i++)
	{
		fread(indices,sizeof(int),4,pFile);
		m_Polygons.SetAt(i,new CPolygon3D(m_Vertices[indices[0]],m_Vertices[indices[1]],
			                          m_Vertices[indices[2]],m_Vertices[indices[3]]));
	}
	fclose(pFile);*/
}

//*********************************************
// ReadFileObj
// v x y z
// f i1//n1 i2 i3 ... in (1-based)
// f 1348//2698 1346//2696 1415//2713 1420//2715
//*********************************************
void CPolygonMesh3d::ReadFileObj(char *filename)
{
	TRACE("Reading obj mesh\n");
	TRACE("  file : %s\n",filename);
	Free();
	FILE *pStream;
	const unsigned int MaxLine = 255;
	char *pLine = new char[255];
	pStream = fopen(filename,"r");
	ASSERT(pStream != NULL);
	float x,y,z;
	int index,n;
	char index_ascii[255], n_ascii[255];
	char *pTmp;
	while(fgets(pLine,MaxLine,pStream))
	{
		pTmp = pLine;
		// Vertex
		if(pTmp[0] == 'v' && pTmp[1] == ' ')
		{
			sscanf(pTmp,"v %g %g %g",&x,&y,&z);
			m_Vertices.Add(new CPolygonVertex3d(x,y,z));
		}
		// Face
		if(pTmp[0] == 'f' && pTmp[1] == ' ')
		{
			pTmp += 2; // i.e. after 'f '
			CPolygon3d *pPolygon = new CPolygon3d;
			if(strstr(pTmp,"//"))
				while(sscanf(pTmp,"%d//%d",&index,&n))
				{
					_itoa_s(index,index_ascii,10);
					_itoa_s(n,n_ascii,10);
					pTmp += (2 + strlen(index_ascii) + strlen(n_ascii));
					pPolygon->Add(m_Vertices[index-1]);
					if(strlen(pTmp) < 3)
						break;
					else
						pTmp += 1;
				}
			else
				while(sscanf(pTmp,"%d",&index))
				{
					_itoa_s(index,index_ascii,10);
					pTmp += strlen(index_ascii);
					pPolygon->Add(m_Vertices[index-1]);
					if(strlen(pTmp) < 3)
						break;
					else
						pTmp += 1;
				}
			ASSERT(pPolygon->GetSize() > 2);
			m_Polygons.Add(pPolygon);
		}
	}
	delete [] pLine;
	fclose(pStream);
	TRACE("Read obj file (%d polygons)\n",m_Polygons.GetSize());
}




void CPolygonMesh3d::SetVerticesSize(int size)
{
	m_Vertices.SetSize(size);
}

void CPolygonMesh3d::SetVertexAt(int i, float x, float y, float z)
{
m_Vertices.SetAt(i,new CPolygonVertex3d(x,y,z));
}

void CPolygonMesh3d::SetPolygonsSize(int size)
{
  m_Polygons.SetSize(size);
}

void CPolygonMesh3d::SetScale(CVector3d *pVector)
{
 m_Transform.SetScale(pVector);
}

float CPolygonMesh3d::GetScaleX()
{
 return (float)m_Transform.GetScale()->Getx();
}

float CPolygonMesh3d::GetScaleY()
{
return (float)m_Transform.GetScale()->Gety();
}

float CPolygonMesh3d::GetScaleZ()
{
return (float)m_Transform.GetScale()->Getz();
}

void CPolygonMesh3d::SaveFilePly(const char *filename)
{
	FILE *pFile = fopen(filename,"wt");
	if(!pFile)
	{
		TRACE("** unable to open file %s for writing\n",filename);
		return;
	}	
	int NbVertex = m_Vertices.GetSize();
	int NbPolygons = m_Polygons.GetSize();
	
	fprintf(pFile, "ply\n");
	fprintf(pFile, "format ascii 1.0\n");
	fprintf(pFile, "element vertex %d\n", NbVertex);
	fprintf(pFile, "property float x\n");
	fprintf(pFile, "property float y\n");
	fprintf(pFile, "property float z\n");
	fprintf(pFile, "element face %d\n", NbPolygons);
	fprintf(pFile, "property list uchar int vertex_indices\n");
	fprintf(pFile, "end_header\n");

	// output vertices
	int i;
	for(i=0;i<NbVertex;i++)
	{
		m_Vertices[i]->SetFlag(i);
		fprintf(pFile, "%g %g %g\n",m_Vertices[i]->x(), m_Vertices[i]->y(), m_Vertices[i]->z());
	}
	for(i=0;i<NbPolygons;i++) {
		int d = m_Polygons[i]->GetSize();
		fprintf(pFile, "%d ", d);
		for(int j=0;j<d;j++) {
			int index = m_Polygons[i]->v(j)->GetFlag();
			fprintf(pFile, "%d ", index);
		}
		fprintf(pFile, "\n");
	}
	fclose(pFile);
}

long CPolygonMesh3d::NbVertex()
{
	return m_Vertices.GetSize();
}

long CPolygonMesh3d::NbFace()
{
	return m_Polygons.GetSize();
}

void CPolygonMesh3d::ConvertTo075Canonical()
{
	CVector3d v3Max(-1000000.0f,-1000000.0f,-1000000.0f);
	CVector3d v3Min(1000000.0f,1000000.0f,1000000.0f);

	CVector3d v3Centroid;
	CVector3d v3Length;
	CPolygonVertex3d * pVertex;
	float lmax;
	int numVerts = NbVertices();
	for(int i = 0; i < numVerts ; i++)
	{
		pVertex = m_Vertices[i];
		float x = pVertex->Get(0);
		float y = pVertex->Get(1);
		float z = pVertex->Get(2);

		if(x > v3Max.x())
			v3Max.x(x);
		if(y > v3Max.y())
			v3Max.y(y);
		if(z > v3Max.z())
			v3Max.z(z);

		if(x < v3Min.x())
			v3Min.x(x);
		if(y < v3Min.y())
			v3Min.y(y);
		if(z < v3Min.z())
			v3Min.z(z);

	}

	v3Centroid.Set(0.5f*(v3Max.x()+v3Min.x()), 0.5f*(v3Max.y()+v3Min.y()), 0.5f*(v3Max.z()+v3Min.z()));
	v3Length.Set(v3Max.x()-v3Min.x(), v3Max.y()-v3Min.y(), v3Max.z()-v3Min.z());
	lmax = max( v3Length.x(), max(v3Length.y(), v3Length.z()) );

	printf("\n (before conv) Min = (%f, %f, %f) ~ Max = (%f, %f, %f)\n",v3Min.x(), v3Min.y(), v3Min.z(), v3Max.x(), v3Max.y(), v3Max.z());
	v3Max.Set(-1000000.0f,-1000000.0f,-1000000.0f);
	v3Min.Set(1000000.0f,1000000.0f,1000000.0f);
	for(int i = 0; i < numVerts ; i++)
	{
		pVertex = m_Vertices[i];
		float x = pVertex->Get(0);
		float y = pVertex->Get(1);
		float z = pVertex->Get(2);

		pVertex->Set(0.75*(x-v3Centroid.x())/lmax + 0.75f/2.0f,0.75*(y-v3Centroid.y())/lmax + 0.75f/2.0f,0.75*(z-v3Centroid.z())/lmax + 0.75f/2.0f);

		x = pVertex->Get(0);
		y = pVertex->Get(1);
		z = pVertex->Get(2);

		if(x > v3Max.x())
			v3Max.x(x);
		if(y > v3Max.y())
			v3Max.y(y);
		if(z > v3Max.z())
			v3Max.z(z);

		if(x < v3Min.x())
			v3Min.x(x);
		if(y < v3Min.y())
			v3Min.y(y);
		if(z < v3Min.z())
			v3Min.z(z);

	}
		printf("\n (after conv) Min = (%f, %f, %f) ~ Max = (%f, %f, %f)\n",v3Min.x(), v3Min.y(), v3Min.z(), v3Max.x(), v3Max.y(), v3Max.z());


}
