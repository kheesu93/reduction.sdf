#ifndef __KERNELS________________
#define __KERNELS________________


__global__ void DistanceFromPointToTriangle(float * d_Dist2, char * d_Type, 
											float	Px, float Py, float Pz,  
											float * d_T0x, float * d_T0y, float * d_T0z, 
											float * d_T1x, float * d_T1y, float * d_T1z, 
											float * d_T2x, float * d_T2y, float * d_T2z, unsigned long long N);

#endif