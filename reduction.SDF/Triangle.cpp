#include "Triangle.h"


/*
Triangle::Triangle()
{
	//m_Tid = NULL;

}

Triangle::~Triangle()
{
	//SAFEDELETEARRAY(m_Tid);
}
*/
__host__ __device__ Vertex * Triangle::v(unsigned int index, Vertex * verts)
{
	unsigned int a = m_ui[index];
	return &verts[a];
}