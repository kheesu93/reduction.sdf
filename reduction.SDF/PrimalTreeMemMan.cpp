//****************************************************************************************
// Taejung Park (taejung.park@gmail.com)
// Created : 2010. 02. 04.
// Modified :
// SDFCUDA.h
// Contains declarations of wrap functions
//****************************************************************************************

#include "PrimalTreeMemMan.h"

unsigned int PrimalTreeMemMan::GetAmountAvailableMemory(void)
{
	return (unsigned int) sizeof(CPrimalNode) * (m_size -m_current ) ;
}
unsigned int	PrimalTreeMemMan::GetAmountUsedMemory(void)
{
	return (unsigned int) sizeof(CPrimalNode) * (m_current ) ;
}


bool PrimalTreeMemMan::IsEmpty()
{
	if(m_current == m_size)
		return true;
	else 
		return false;
}

bool	PrimalTreeMemMan::AllocNodes(int size)
{
	m_pNodes = new CPrimalNode[size];
	m_size = size;
	if(m_pNodes == NULL)
		return false;
	else
		return true;
}


bool PrimalTreeMemMan::	DeleteAll(void)
{
	if(m_pNodes != NULL)
	{
		delete [] m_pNodes;
		m_pNodes = NULL;
	}
	return true;

}

unsigned int PrimalTreeMemMan::GetAvailableNumNodes(void)
{
	return (unsigned int) abs((int)m_current - (int)m_size);
}


CPrimalNode		*	PrimalTreeMemMan::NewNode(void)
{
	if(m_current<m_size)
		return &m_pNodes[m_current++];
	else
		return NULL;			// out of mem
}



