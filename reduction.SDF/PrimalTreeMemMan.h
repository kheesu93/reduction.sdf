//****************************************************************************************
// Taejung Park (taejung.park@gmail.com)
// Created : 2010. 02. 04.
// Modified :
// SDFCUDA.h
// Contains declarations of wrap functions
//****************************************************************************************

#ifndef _PrimalTreeMemMan___
#define _PrimalTreeMemMan___

#include ".\GeoSynEngine Console\PrimalNode.h"

class PrimalTreeMemMan
{
	CPrimalNode * m_pNodes;
	unsigned int  m_size, m_current;

public:
	bool						AllocNodes(int size);
	bool						DeleteAll(void);
	bool						IsEmpty();
	unsigned int			GetAvailableNumNodes(void);
	unsigned int			GetAmountAvailableMemory(void);
	unsigned int			GetAmountUsedMemory(void);

	CPrimalNode		*	NewNode(void);

	

	PrimalTreeMemMan()
	{
		m_pNodes = NULL;
		 m_size = m_current= 0;
	}

	PrimalTreeMemMan(int size)
	{
		 m_size = m_current= 0;
		AllocNodes(size);
	}
	~PrimalTreeMemMan()
	{
		if(m_pNodes != NULL)
			delete m_pNodes;
		m_pNodes = NULL;
	}

};

#endif
